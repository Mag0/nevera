addEventListener('load',inicio);
	var abierto = false;
function inicio(){
	// obtengo la imagen
	var nevera = document.getElementById("frigo");
	// variable abierto

	// Extraigo la longitud del array de localstorage
	var longStorage = 0;

	if (longStorage==0){
		alert ("Lanevera esta vacia"+"\n"+"!LLenala!");
		// muestro el formulario correcto
		document.getElementById("lista").setAttribute("hidden","true");
		document.getElementById("productos").removeAttribute("hidden");
		// cargo el storage con productos
		localStorage.setItem("Leche",0);
		localStorage.setItem("Agua",0);
		localStorage.setItem("manzanas",0);
		localStorage.setItem("Queso",0);
		localStorage.setItem("Pizza",0);
		localStorage.setItem("Yogurt",0);
		localStorage.setItem("Pavo",0);
		// lleno la tabla
		llenartabla("tablaPro");
	}else{
		// lleno la tabla
		llenartabla("tablaPro");
	}
	// Si hacemos click en la imagen de la nevera 
	lleno = document.getElementById('frigo');
	lleno.addEventListener('click',function(e) {
		// si abierto es falso
		if(abierto){
			e.preventDefault();		
		}else{
			abierto = true;
			let tabla = document.getElementById("tablaLis");
			while ( tabla.childNodes.length >= 1 ){
				tabla.removeChild( tabla.firstChild );
			}			
			// cambiamos la imagen de la nevera por una abierta
			nevera.setAttribute('src', 'lleno.jpg');
			// muestro el formulario correcto
			document.getElementById("productos").setAttribute("hidden","true");
			document.getElementById("lista").removeAttribute("hidden");				
			// Lleno la tabla
			llenartabla("tablaLis");
		}

	});

	//si hacemos doble click en la imagen de la nevera llena 
	lleno.addEventListener('dblclick',function(e) {
		let tabla = document.getElementById("tablaPro");
		while ( tabla.childNodes.length >= 1 ){
			tabla.removeChild( tabla.firstChild );
		}	
		// si abierto es falso 		
		if(abierto==false){
			e.preventDefault();		
		}else{			
			nevera.setAttribute('src', 'frigo.jpg');
			// muestro el formulario correcto
			document.getElementById("lista").setAttribute("hidden","true");
			document.getElementById("productos").removeAttribute("hidden");
			llenartabla("tablaPro");
			abierto = false;
		}		
	});

	compra = document.getElementById('comprar');
	compra.addEventListener('click',function() {
		actualizado();
		llenartabla(tablaPro);
	});

	actualizar = document.getElementById('actualiza');
	actualizar.addEventListener('click',function() {
		actualizado();
		let tabla = document.getElementById("tablaLis");
		llenartabla(tablaLis);
	});


	//boton que habilita el  formulario para añadir productos nuevos 
	npro = document.getElementById('pNuevo');
	npro.addEventListener('click',function() {
		document.getElementById("productosNuevos").removeAttribute("hidden");
	});

	// Boton que ingresa productos nuevos
	ingresa = document.getElementById('incluir');
	ingresa.addEventListener('click',function() {
		// extraigo el nombre del producto
		let nombre = document.getElementById("nomProducto").value ;
		// extraigo la cantidad del producto
		let valor =document.getElementById("numeroPro").value;
		// Creo el producto
		localStorage.setItem(nombre,valor);
		// Muestro un aler
		alert("nuevo Producto añadido");
		// Oculto el formulario de añadir productos nuevos
		document.getElementById("productosNuevos").setAttribute("hidden","true");
		// Extraigo la tabla y la limpio
		let tabla = document.getElementById("tablaPro");
		while ( tabla.childNodes.length >= 1 ){
			tabla.removeChild( tabla.firstChild );
		}
		llenartabla("tablaPro");
	});

}
function modificaStorage (nombre) {
	// extraigo el nuevo valor
	let valor =document.getElementById("n"+nombre).value;
	localStorage.setItem(nombre,valor);	
}
function llenartabla(tbla) {
	for (var i = 0; i < localStorage.length; i++) {
		// Extraigo el nombre del storage
		nombre = localStorage.key(i);
		// extraigo el valor del storage
		valor = localStorage.getItem(nombre);

		let tr = document.createElement("tr");
		// creo td checkbox
		let td = document.createElement("td");
		// creo input checkbox
		let input = document.createElement("input");
		// creo td checkbox
		let tdcaja = document.createElement("td");
		// creo input checkbox
		let inputCaja = document.createElement("input");
		// creo el texto 
		let texto = document.createTextNode(nombre);
		// añado atributos al input del checkbox
		input.setAttribute("type","checkbox");
		input.setAttribute("id",nombre);
		// añado atributos al input caja
		inputCaja.setAttribute("type","number");
		inputCaja.setAttribute("id","n"+nombre);
		inputCaja.setAttribute("value",valor);

		// introduzco los elementos en la tabla
		let tabla = document.getElementById(tbla);
		tabla.appendChild(tr);
		tr.appendChild(td);		
		td.appendChild(input);
		tr.appendChild(tdcaja);		
		tdcaja.appendChild(inputCaja);
		td.appendChild(texto);
	};
}

function actualizado() {
	let cadena = "";
	let todos = document.getElementsByTagName("input");
	for (var i = 0; i < todos.length; i++) {
		if (todos[i].checked){
			let p = todos[i].getAttribute("id");
			modificaStorage(p);
			cadena = cadena +" "+ p;
		}
	};
	alert(cadena+" actualizado");	
}